.DEFAULT_GOAL := all

all:
		@if [ ! -d "node_modules" ]; then \
			$(MAKE) install ;\
		fi
		@$(MAKE) drupal-ci-local

.PHONY: install
install:
		# Verificar si la carpeta node_modules no existe
		npm i gitlab-ci-local;

.PHONY: drupal-ci-local
drupal-ci-local: node_modules/.bin/gitlab-ci-local
		./node_modules/.bin/gitlab-ci-local --remote-variables git@git.drupal.org:project/gitlab_templates=includes/include.drupalci.variables.yml=1.0.x --variable="_GITLAB_TEMPLATES_REPO=project/gitlab_templates"
